import { Injectable } from '@angular/core';
import { URL_SERVICES } from './../config/config';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';
import { map } from 'rxjs/operators';

@Injectable()
export class GlobalService {

    public characters: any;
    public RandomChar: any;
    public Detail: any;

    constructor(
        public http: HttpClient
    ) {}

    getCharacterRandom() {
        const numberRandom = Math.floor(Math.random() * 10) + 1;
        const url = `${URL_SERVICES}/character/?page=${numberRandom}`;

        return this.http.get(url)
        .map( (res: any) => {
            this.RandomChar = res.results;
        });
    }

    getCharacterByPage(page: number) {
        const url = `${URL_SERVICES}/character/?page=${page}`;

        return this.http.get(url)
        .map( (res: any) => {
            this.characters = res.results;
        });
    }

    getCharacterScroll(page: number) {
        const url = `${URL_SERVICES}/character/?page=${page}`;

        return this.http.get(url)
            .map((res: any) => {
                res.results.forEach(element => {
                    this.characters.push(element);
                });
            });
    }

    getDetailCharacter(id: string) {
        const url = `${URL_SERVICES}/character/${id}`;
        return this.http.get(url)
        .map((res: any) => {
            this.Detail = res;
        });
    }
}
