import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/pages/home/home.component';
import { DetailComponent } from './components/pages/detail/detail.component';
import { CharactersComponent } from './components/pages/characters/characters.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
    { path: 'detail/:id', component: DetailComponent },
    { path: 'characters', component: CharactersComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
