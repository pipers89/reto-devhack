import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {

  @Input('characters') character;

  show = false;

  constructor() { }

  ngOnInit() {
    setTimeout( () => {
      this.show = true;
    }, 2000);
  }

}
