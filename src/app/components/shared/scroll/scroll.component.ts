import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../../service/global.service';

@Component({
  selector: 'app-scroll',
  templateUrl: './scroll.component.html',
  styleUrls: ['./scroll.component.css']
})
export class ScrollComponent implements OnInit {

  scroll = 1;

  constructor(
    public _globalService: GlobalService
  ) { }

  ngOnInit() {
  }

  onScroll() {
    this.scroll = this.scroll + 1;
    this._globalService
    .getCharacterScroll(this.scroll)
    .subscribe();
  }

}
