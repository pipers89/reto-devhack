import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../../service/global.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  loading = true;
  public characters: any[] = [];

  constructor(
    public _globalService: GlobalService
   ) {
    this._globalService.getCharacterRandom().subscribe( () => {
      // this.getThreeRamdom();
      const randomNumber = Math.floor(Math.random() * 21) + 4;
      this.characters = this._globalService.RandomChar.slice(randomNumber - 3, randomNumber);
      this.loading = false;
    });
   }

  ngOnInit() {}

}
