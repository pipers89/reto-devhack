import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../../service/global.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  constructor(
    public _globalService: GlobalService,
    private router: ActivatedRoute
    ) {
      this.router.params
      .subscribe(param => {
        this._globalService.getDetailCharacter(param['id']).subscribe();
      });
     }

  ngOnInit() {
  }

}
