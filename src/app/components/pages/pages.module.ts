import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// importando componentes que pertenecen a este modulo
import { HomeComponent } from './home/home.component';
import { DetailComponent } from './detail/detail.component';
import { CharactersComponent } from './characters/characters.component';

// importando modulos para poder usar los componentes compartidos
import { SharedModule } from '../shared/shared.module';


@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        SharedModule
    ],
    declarations: [
       HomeComponent,
       DetailComponent,
       CharactersComponent
    ],
    exports: [
        HomeComponent,
        DetailComponent,
        CharactersComponent
    ]
})

export class PagesModule {}
