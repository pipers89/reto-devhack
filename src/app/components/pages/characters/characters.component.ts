import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../../service/global.service';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.css']
})
export class CharactersComponent implements OnInit {

  loading = true;
  public characters: any[] = [];

  constructor(
    public _globalService: GlobalService
  ) {
      this._globalService.getCharacterByPage(1)
      .subscribe((res: any) => {
        console.log(res);
        this.characters = this._globalService.characters;
        this.loading = false;
      });
   }

  ngOnInit() {
  }

}
